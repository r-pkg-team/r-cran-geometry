Source: r-cran-geometry
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-geometry
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-geometry.git
Homepage: https://cran.r-project.org/package=geometry
Standards-Version: 4.7.1
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-magic,
               r-cran-rcpp,
               r-cran-lpsolve,
               r-cran-linprog,
               r-cran-rcppprogress
Testsuite: autopkgtest-pkg-r

Package: r-cran-geometry
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R mesh generation and surface tesselation
 This GNU R package makes the qhull library (www.qhull.org)
 available in R, in a similar manner as in Octave and MATLAB. Qhull
 computes convex hulls, Delaunay triangulations, halfspace
 intersections about a point, Voronoi diagrams, furthest-site
 Delaunay triangulations, and furthest-site Voronoi diagrams. It
 runs in 2-d, 3-d, 4-d, and higher dimensions. It implements the
 Quickhull algorithm for computing the convex hull. Qhull does not
 support constrained Delaunay triangulations, or mesh generation of
 non-convex objects, but the package does include some R functions
 that allow for this. Currently the package only gives access to
 Delaunay triangulation and convex hull computation.
